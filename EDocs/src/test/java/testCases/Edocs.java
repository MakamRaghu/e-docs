package testCases;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import bussinessComponent.BussinessComponent;
import bussinessComponent.ReadExcelDataUsingPOI;

public class Edocs extends BussinessComponent{
	@Parameters("BROWSER")
	@BeforeTest(alwaysRun=true)
	public void callingBrowser(String BROWSER) throws IOException, InterruptedException {
		FileInputStream file = new FileInputStream(path_propertyFile);
		pro.load(file);
		CalBrowser(pro.getProperty("URL"),BROWSER);
		OkataLogin(pro.getProperty("OktaUserID"),pro.getProperty("OktaPwd"));
	}
	//@Parameters("BROWSER")
	//@Test(priority=1)
	public void AccountingSearch(String BROWSER) throws IOException, ParseException, InterruptedException {
		ReadExcelDataUsingPOI read = new ReadExcelDataUsingPOI("Edocs.xlsx", "Customer");
		for(int i=0;i<2;i++) {
		clickElement(Accounting_Linktext);
		Thread.sleep(2000);
		waitUntill_visibilityOfElement(CustomerNumOrOrder_TxtBox, 15);
		SendTextIntoTextBox(CustomerNumOrOrder_TxtBox, read.readData(i, 0));
		Thread.sleep(2000);
		String AccountstartTime = trackTime();
		clickElement(AddView_Button);
		Thread.sleep(2000);
		try {
			Boolean resul = driver.findElement(By.xpath("//div[text()='Folder does not exist. Please re-check and try again']")).isDisplayed();
			if(resul.TRUE) {
				System.out.println(BROWSER+" No data Found for this record "+read.readData(i, 0));
				Thread.sleep(2000);
				clickElement(By.xpath("//button[text()='ok']"));
				Thread.sleep(2000);
			}
		}
		catch(Exception e) {
		waitUntill_visibilityOfElement(tablelist, 200);
		String AccountendTime = trackTime();
		String AccounttakenTime = timeStampDiff(AccountstartTime,AccountendTime);
		String TableText = getTextUsingLocator(By.className("dataTables_info"));
		String[] count = TableText.split(" ");
		System.out.println(BROWSER+" Customer No "+read.readData(i, 0)+" Time taken to get Customer data when user search from accounting tab "+AccounttakenTime +" Count of records is "+count[5]);
		
		waitUntill_visibilityOfElement(Search_Linktext, 200);
		clickElement(Search_Linktext);
		clickElement(ApplicationArea_DrpDown);
		clickElement(Accounting_Option);
		clickElement(RootFolder_DrpDown);
		clickElement(Customers_Option);
		SendTextIntoTextBox(SearchCustomerOrOrder_TxtBox, read.readData(i, 0));
		String SearchstartTime = trackTime();
		clickElement(AddView_Button);
		waitUntill_visibilityOfElement(tablelist, 200);
		String SearchendTime = trackTime();
		String SearchTableText = getTextUsingLocator(By.className("dataTables_info"));
		String[] searchTextcount = SearchTableText.split(" ");
		String SearchtakenTime = timeStampDiff(SearchstartTime,SearchendTime);
		System.out.println(BROWSER+" Customer No "+read.readData(i, 0)+ " Time taken to get Customer data when user search from Search Criteria "+SearchtakenTime +" Count of records is "+searchTextcount[5]);
			}
		}
	}
	@Parameters("BROWSER")
	@Test(priority=2)
	public void OracleSearch(String BROWSER) throws IOException, ParseException, InterruptedException {
		ReadExcelDataUsingPOI read = new ReadExcelDataUsingPOI("Edocs.xlsx", "Order");
		for(int i=0;i<9;i++) {
		Thread.sleep(2000);
		clickElementUsingJS(Oracle_Linktext);
		Thread.sleep(2000);
		SendTextIntoTextBox(CustomerNumOrOrder_TxtBox, read.readData(i, 0));
		Thread.sleep(2000);
		String OraclestartTime = trackTime();
		clickElement(AddView_Button);
		Thread.sleep(2000);
		try {
			Boolean resul = driver.findElement(By.xpath("//div[text()='Folder does not exist. Please re-check and try again']")).isDisplayed();
			if(resul.TRUE) {
				System.out.println(BROWSER+" No data Found for this record "+read.readData(i, 0));
				Thread.sleep(2000);
				clickElement(By.xpath("//button[text()='ok']"));
				Thread.sleep(2000);
			}
		}
		catch(Exception e) {
			waitUntill_visibilityOfElement(tablelist, 300);
			String OraclendTime = trackTime();
			String TableText = getTextUsingLocator(By.className("dataTables_info"));
			String[] count = TableText.split(" ");
			String OracletakenTime = timeStampDiff(OraclestartTime,OraclendTime);
			System.out.println(BROWSER+" Order No "+read.readData(i, 0)+" Time taken to get Orders data when user search from Oracle tab "+ OracletakenTime +" Count of records is "+count[5]);
			
			clickElement(Search_Linktext);
			clickElement(ApplicationArea_DrpDown);
			clickElement(Oracle_Option);
			clickElementUsingJS(RootFolder_DrpDown);
			clickElementUsingJS(Order_Option);
			SendTextIntoTextBox(SearchCustomerOrOrder_TxtBox, read.readData(i, 0));
			String SearchstartTime = trackTime();
			clickElement(AddView_Button);
			waitUntill_visibilityOfElement(tablelist, 200);
			String SearchendTime = trackTime();
			String SearchTableText = getTextUsingLocator(By.className("dataTables_info"));
			String[] searchTextcount = SearchTableText.split(" ");
			String SearchtakenTime = timeStampDiff(SearchstartTime,SearchendTime);
			System.out.println(BROWSER+" Order No "+read.readData(i, 0)+" Time taken to get Orders data when user search from search Criteria "+SearchtakenTime  +" Count of records is "+searchTextcount[5]);
			}
		}
	}
	
}
