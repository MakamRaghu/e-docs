package pageElements;

import org.openqa.selenium.By;

public interface Fussion_And_Oracle {
	
	// okta Login
	public static By oktaUserID_TextBox = By.xpath("//input[@id='okta-signin-username']");
	public static By oktaPasswd_TextBox = By.xpath("//input[@id='okta-signin-password']");
	public static By oktaLogin_Button = By.xpath("//input[@id='okta-signin-submit']");
	
	//Fussion Login
	public static By FUName = By.id("userid");
	public static By FUPwd = By.id("password");
	public static By FSignBtn = By.id("btnActive");
	
	//Oracle Login
	public static By OMusername = By.id("username");
	public static By OMpassword = By.id("password");
	public static By OMsignInBtn = By.id("signin");
	public static By HomePage = By.xpath("(//a[@title='Home'])[1]");
	
	public static By Accounting_Linktext = By.xpath("(//a[text()='Accounting'])[1]");
	public static By Oracle_Linktext = By.xpath("(//a[text()='Oracle'])[1]");
	public static By Search_Linktext = By.xpath("//a[@class='search-icon']");
	public static By CustomerNumOrOrder_TxtBox = By.id("RootFolerValue");
	public static By AddView_Button = By.xpath("//button[@type='submit']");
	public static By ApplicationArea_DrpDown = By.xpath("//button[@title='Choose Application Area']");
	public static By RootFolder_DrpDown = By.xpath("//button[@title='Choose Root Folder']");
	public static By SearchCustomerOrOrder_TxtBox = By.id("txtSearchInput");
	public static By tablelist = By.id("order_list_wrapper");
	public static By Accounting_Option = By.xpath("//a//span[text()='Accounting']");
	public static By Customers_Option = By.xpath("//a//span[text()='Customers']");
	public static By Oracle_Option = By.xpath("//a//span[text()='Oracle']");
	public static By Order_Option = By.xpath("//a//span[text()='Orders']");
	
}
