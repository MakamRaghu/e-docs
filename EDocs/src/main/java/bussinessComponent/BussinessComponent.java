package bussinessComponent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;

import pageElements.Fussion_And_Oracle;

public class BussinessComponent extends ReusableFunctions implements Fussion_And_Oracle{
	public void FussionLogin(String UserNm,String Pwd) throws FileNotFoundException, IOException {
		pro.load(new FileInputStream(path_propertyFile));
		SendTextIntoTextBox(FUName, UserNm);
		SendTextIntoTextBox(FUPwd, Pwd);
		clickElement(FSignBtn);
	}
	
	public void OkataLogin(String UserId, String Pwd) throws IOException, InterruptedException {
		SendTextIntoTextBox(oktaUserID_TextBox, UserId);
		Thread.sleep(4000);
		SendTextIntoTextBox(oktaPasswd_TextBox, Pwd);
		waitUntill_visibilityOfElement(oktaLogin_Button, 200);
		Thread.sleep(4000);
		clickElement(oktaLogin_Button);
		driver.findElement(By.name("answer")).sendKeys("kohli");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		Thread.sleep(2000);
	}
	public void clickCorrespondingRow(String Val) {
		List<WebElement> lis = driver.findElements(By.xpath("(//table[@class='oj-table-element oj-component-initnode'])[2]//tr/td[10]"));
		for(WebElement li : lis) {
			if(li.getText().equals(Val)) {
				System.out.println(li.getSize());
				driver.findElement(By.xpath("(//table[@class='oj-table-element oj-component-initnode'])[2]//tr/td[2]")).click();
			}
		}
	}
	public String timeStampDiff(String dateStart,String dateStop) throws ParseException {
	    SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	    Date d1 = null;
	    Date d2 = null;
	    d1 = format.parse(dateStart);
		d2 = format.parse(dateStop);
	    long diff = d2.getTime() - d1.getTime();
	    long diffSeconds = diff / 1000 % 60;
	    long diffMinutes = diff / (60 * 1000) % 60;
	    long diffHours = diff / (60 * 60 * 1000);
	    return diffHours + " hours."+diffMinutes + " minutes."+diffSeconds + " seconds.";
}
	
	public String trackTime() {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		return timeStamp;
	}

}
