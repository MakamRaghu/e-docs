package bussinessComponent;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ReadExcelDataUsingPOI {
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	public ReadExcelDataUsingPOI(String filename, String sheetName) throws  IOException {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\src/main/java/testData\\"+ filename);
		workbook = new XSSFWorkbook(fis);
		sheet = workbook.getSheet(sheetName);
		
	}
	public String readData(int i, int j) {
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		String exformulaString=null;
		Row row = sheet.getRow(i);
		Cell cell = row.getCell(j);
		String cellvalue=null;
		switch(cell.getCellType()) {
		case STRING:
		cellvalue = cell.getStringCellValue();
		break;
		
		case NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy");
				cellvalue = dateFormat.format(cell.getDateCellValue());
			} else {
				DataFormatter fmt = new DataFormatter();
				cellvalue = fmt.formatCellValue(cell);
			}
			
         break;

		case FORMULA:
			 evaluator.evaluateInCell(cell);
		    	switch(cell.getCellType()) {
		    	case STRING:
		            if(row.getRowNum()==i && cell.getColumnIndex()==j) {
		            	exformulaString=cell.getStringCellValue();
		            	break;
		            }
		            break;
		        case NUMERIC:
		        	double numval=cell.getNumericCellValue();
		        	cellvalue = Double.toString(numval);
		        	//cell.getNumericCellValue();
		            break;
		        case FORMULA:
		         	if (DateUtil.isCellDateFormatted(cell)) {
		                SimpleDateFormat dateFormat = new SimpleDateFormat("d/m/yy");
		                System.out.print(dateFormat.format(cell.getDateCellValue()) + "\t\t");
		            } else {
		                System.out.print(cell.getNumericCellValue() + "\t\t");
		            }
		            break;
		       default:
		            cell.getHyperlink();
		            break;
		    	}
		
		default:
		cellvalue = cell.getStringCellValue();
		break;
		}
		
		return cellvalue;
	}
}
