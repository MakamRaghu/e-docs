package bussinessComponent;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class InitialiseBrowser {
	public WebDriver driver;
	public Properties pro = new Properties();
	public String path_propertyFile = System.getProperty("user.dir")+"/src/main/java/configurator/Config.properties";
	Logger log= Logger.getLogger(InitialiseBrowser.class);
	public void CalBrowser(String URL,String BROWSER) throws IOException {
		System.out.println("    ");
		FileInputStream file = new FileInputStream(path_propertyFile);
		pro.load(file);
		
		switch (BROWSER) {
		case "Chrome":
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			log.info("launching chrome broswer");
			System.out.println("------------------------------------Chrome Browser initiated---------------------------------------");
			getURL_Maxmise(URL);
			break;
		case "FireFox":
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+pro.getProperty("FIREFOXPATH"));
			driver = new FirefoxDriver();
			log.info("launching firefox broswer");
			System.out.println("------------------------------------Mozilla Browser initiated---------------------------------------");
			getURL_Maxmise(URL);
			break;
		case "headless":
			 driver = new HtmlUnitDriver();
			 System.out.println("------------------------------------Headless Browser initiated---------------------------------------");
			 break;
		default:
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+pro.getProperty("IEPATH"));
			driver = new InternetExplorerDriver();
			getURL_Maxmise(URL);
			break;
		}
	}
	private void getURL_Maxmise(String URL) {
		driver.get(URL);
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		driver.manage().window().maximize();
	}
	
	@AfterTest(alwaysRun=true)
	public void tearDown() {
		if(driver != null)
		{
			System.out.println("----------------------------Browser Closing  ---------------------------------------");
			driver.quit();
			driver = null;
		}
	}

}
