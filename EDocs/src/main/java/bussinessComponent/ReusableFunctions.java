package bussinessComponent;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.io.Files;


public class ReusableFunctions extends InitialiseBrowser{
	
	/*
	 * Author : M Raghu Vardhan
	 * Scripted Date : 26 Nov 2019
	 * Description : Script for Actions have been added
	 * ClickTextBox, Clear Textbox, Send Text into Textbox
	 * selectValueFromDropDownUsingIndex, selectValueFromDropDownUsingValue, selectValueFromDropDownUsingVisibleText
	 * clickLinkText, takeScreenShoot
	 */
	
	
	//******************************************Textbox**********************************************//
	public void ClickTextBox(By locator) throws IOException {
		try {
				waitUntill_visibilityOfElement(locator, 200);
				driver.findElement(locator).click();
				takeScreenShoot(locator);
		} catch (Exception e) {
			takeScreenShoot(locator);
			System.out.println(e);
		}
	}

	public void ClearTextBox(By locator) {
		try {
				waitUntill_visibilityOfElement(locator, 200);
				driver.findElement(locator).click();
				driver.findElement(locator).clear();
				takeScreenShoot(locator);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void SendTextIntoTextBox(By locator, String textValue) throws IOException {
		try {
				waitUntill_visibilityOfElement(locator, 200);
				driver.findElement(locator).click();
				driver.findElement(locator).clear();
				driver.findElement(locator).sendKeys(textValue);
				takeScreenShoot(locator);
		} catch (Exception e) {
			takeScreenShoot(locator);
			System.out.println(e);
		}

	}
	
	public void getTextLength(By locator) {
		 int sizeOfTextValue = driver.findElement(locator).getText().length();
		 System.out.println("Length of text value is "+sizeOfTextValue);
	}
	
	public String getTextUsingLocator(By locator) {
		String text = driver.findElement(locator).getText();
		return text;
	}
	
	//******************************************Drop Down**********************************************//
	
	public void selectValueFromDropDownUsingIndex(By locator, int index) throws InterruptedException, IOException {
		try {
				waitUntill_visibilityOfElement(locator, 200);
				Select sel = new Select(driver.findElement(locator));
				sel.selectByIndex(index);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void selectValueFromDropDownUsingValue(By locator, String value) throws InterruptedException, IOException {
		try {
			waitUntill_visibilityOfElement(locator, 200);
			Select sel = new Select(driver.findElement(locator));
			sel.selectByValue(value);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void selectValueFromDropDownUsingVisibleText(By locator, String text) throws InterruptedException, IOException {
		try {
				waitUntill_visibilityOfElement(locator, 200);
				Select sel = new Select(driver.findElement(locator));
				sel.selectByVisibleText(text);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void getData_Into_List_And_Select(By locator, String option) {
		List<WebElement> getData =driver.findElements(locator);
		for(WebElement li : getData) {
			try {
				if (li.getText().equals(option)) {
					li.click();
				}
			} catch (Exception e) {
				System.out.println(e + " No option available given by you in dropdown ");
			}
		}
	}
	//******************************************link text**********************************************//
	
	public void clickLinkText(String text) throws InterruptedException {
		try {
				waitUntill_visibilityOfElement(By.linkText(text),200);
				driver.findElement(By.linkText(text)).click();
				takeScreenShoot(By.linkText(text));
			
		} catch (Exception e) {
		}
	}

	//*****************************************Screenshot********************************************//
	public void takeScreenShoot(By locator) throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MMM.dd HH:mm:ss");
		Date date = new Date();
		if (pro.getProperty("SCREENSHOT").equals("yes")) {
			WebElement ele = driver.findElement(locator);
			File file = new File(System.getProperty("user.dir") + "\\src\\main\\java\\com\\CS\\ScreenShots\\"+dateFormat.format(date)+"\\");
			file.mkdir();
			File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			Files.copy(screenshotFile, new File(file + "\\" + ele.getText() + ".png"));
			log.info("Took screenshort for the page and stored in "+file);
		}
	}
	
	//*****************************************Click Element any locator*******************************************//
	
	public void clickElement(By locator) throws IOException {
		try {
				waitUntill_visibilityOfElement(locator, 300);
				driver.findElement(locator).click();
				takeScreenShoot(locator);
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	//*****************************************Convertions from one data type to another datatype*******************************************//
	
	public int convert_String_To_Int(By locator) {
		int number = 0;
		
		String textvalue = driver.findElement(locator).getText();
		try {
			 number = Integer.parseInt(textvalue);
		}
		catch(NumberFormatException e) {
			log.info(e+"  Unknown Format");
		}
		return number;
	}
	public int convert_String_To_Int(String textval) {
		int number = 0;
		
		String textvalue = textval;
		try {
			 number = Integer.parseInt(textvalue);
		}
		catch(NumberFormatException e) {
			log.info(e+"  Unknown Format");
		}
		return number;
	}
	public double convert_String_To_Double(By locator) {
		double number = 0;
		
		String textvalue = driver.findElement(locator).getText();
		try {
			 number = Double.parseDouble(textvalue);
		}
		catch(NumberFormatException e) {
			log.info(e+"  Unknown Format");
		}
		return number;
	} 
	public double convert_String_To_Double(String textval) {
		double number = 0;
		
		String textvalue = textval;
		try {
			 number = Double.parseDouble(textvalue);
		}
		catch(NumberFormatException e) {
			log.info(e+"  Unknown Format");
		}
		return number;
	}
	
	//*****************************************Alerts Handling*******************************************//
	
	public void acceptAlert() {
		try {
		Alert alert = driver.switchTo().alert();
		alert.accept();
		}
		catch(Exception e) {
			log.info(e+"  No alert found");
		}
	}
	
	public void dismisAlert() {
		try {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
		}
		catch(Exception e) {
			log.info(e+"  No alert found");
		}
	}
	
	public String getAlertText() {
		String textin_Alert = null;
		try {
		Alert alert = driver.switchTo().alert();
		textin_Alert= alert.getText();
		}
		catch(Exception e) {
			log.info(e+"  No alert found");
		}
		return textin_Alert;
	}
	
	public void sendText_Alert(String text) {
		try {
		Alert alert = driver.switchTo().alert();
		alert.sendKeys(text);
		}
		catch(Exception e) {
			log.info(e+"  No alert found");
		}
	}
	
	//*****************************************checkbox and radio btn *******************************************//
	
	public Boolean verifyCheckBox_Checked(By locator) {
		WebElement chkBox = driver.findElement(locator);
		if(chkBox.isSelected()) {
			
			log.info("CheckBox is checked " );
		}
		else {
			log.info("CheckBox is not Checked");
		}
		
		return chkBox.isSelected();
	}
	
	//*****************************************switching to Frame *******************************************//
	
	public void switchToFrame(String frameID) {
		
		if(driver.getPageSource().contains("iframe")) {
		driver.switchTo().frame(frameID);
		log.info("Switching to frame "+frameID);
		}
	}

	public void switchingBackToDefault() {
		driver.switchTo().defaultContent();
		log.info("Switching to default content");
	}
	
	//*****************************************Explicit wait *******************************************//
	
	public void waitUntill_visibilityOfElement(By locator,int timeout) {
		WebDriverWait wait =new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}
	
	public void waitUntill_InvisibilityOfElement(By locator,int timeout) {
		WebDriverWait wait =new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}
	
	public void waitUntill_elementIsClickable(By locator,int timeout) {
		WebDriverWait wait =new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
	}
	//*****************************************JavaScript actions *******************************************//
	public void clickElementUsingJS(By locator) {
			waitUntill_elementIsClickable(locator, 200);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", driver.findElement(locator));
			log.info("Clicked Element using JavaScript --->"+locator);
	}
	
	public void horizontalScroll(int from, int to) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scroll("+from+","+to+")");
	}
	
	public void focusOnElement(By locator) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementById("+"'"+locator+"'"+").focus()");
	}
	
	//*****************************************Action Events *******************************************//
	
	public void moveToElement(By locator) {
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(locator)).build().perform();
	}
	
}
	


